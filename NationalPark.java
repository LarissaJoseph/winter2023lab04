public class NationalPark{
	

	public static void main(String[] args){
	
		java.util.Scanner reader = new java.util.Scanner(System.in);

		Fox[] skulkOfFoxes = new Fox[4];
		
		System.out.println("Enter the default fur color: ");
		String furColorInput = reader.nextLine();
		System.out.println("Enter the default habitat: ");
		String habitatInput = reader.nextLine();
		System.out.println("Enter the default age: ");
		int ageInput = Integer.parseInt(reader.nextLine());
		
		for(int i=0;i<skulkOfFoxes.length;i++){ 
		Fox fox = new Fox(furColorInput,habitatInput,ageInput);//want input of user
		skulkOfFoxes[i] = new Fox(furColorInput,habitatInput,ageInput); 
	
	
		
		//color
		//System.out.println("Enter the color of the fox: ");
		//String theFurColor = reader.nextLine();
		//skulkOfFoxes[i].setFurColor(theFurColor);/*removing the call of set method
		
		
	
		//habitat
		//System.out.println("Enter habitat of the fox: ");
		//String theHabitat = reader.nextLine();
		//skulkOfFoxes[i].setHabitat(theHabitat);

		  
		//age
		//System.out.println("Enter the age of the fox: ");
		//int theAge = Integer.parseInt(reader.nextLine());
		//skulkOfFoxes[i].setAge(theAge);
		}
		
		//printing characteristics of the last fox
		System.out.println("Characteristics of the last fox in the skulk: ");
		System.out.println(skulkOfFoxes[3].getFurColor());
		System.out.println(skulkOfFoxes[3].getHabitat());
		System.out.println(skulkOfFoxes[3].getAge());
		
		System.out.println("New values of the last array with setter methods: ");
		System.out.println("Enter new value for fur color: ");
		String updatedColor = reader.nextLine();
		skulkOfFoxes[3].setFurColor(updatedColor);
		System.out.println("New value of fur color is "+skulkOfFoxes[3].getFurColor());
		
		System.out.println("Enter new value for habitat: ");
		String updatedHabitat = reader.nextLine();
		skulkOfFoxes[3].setHabitat(updatedHabitat);
		System.out.println("New value of fur color is "+skulkOfFoxes[3].getHabitat());
		
		System.out.println("Enter new value for age: ");
		int updatedAge = Integer.parseInt(reader.nextLine());
		skulkOfFoxes[3].setAge(updatedAge);
		System.out.println("New value for age is "+skulkOfFoxes[3].getAge());

		//calling lifeTime() method
		System.out.println("About the first fox's age in the array: ");
		Fox life = new Fox(furColorInput,habitatInput,ageInput);
		life.lifeTime(skulkOfFoxes[0].getAge());
		//calling theHabitat() method
		System.out.println("About the habitat of the first fox in the array: ");
		Fox home = new Fox(furColorInput,habitatInput,ageInput);
		home.theHabitat(skulkOfFoxes[0].getHabitat());
		//calling findNewAge() method
		Fox newAge = new Fox(furColorInput,habitatInput,ageInput);
		newAge.findNewAge(skulkOfFoxes[1].getAge());
	}
			
}



			
			
	
			
			
			
			
			
			
			